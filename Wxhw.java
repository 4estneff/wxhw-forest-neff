/*
* Name:       Forest Neff
* 
* Course:     CS-13, Spring 2022
*
* Date:       4/19/22
*
* Filename:   Wxhw.java
*
* Purpose:    This application takes a zipcode as the first argument on the
*             command line and displays a list of weather conditions for 
*             that zipcode from OpenWeatherAPI.
* 
* Requires:   gson-2.2.4.jar
* 
* Used By:    none
* 
* Based on:   WxStationDEMO.java - (provided)
*/
 
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import java.util.*;
import java.text.*;

import com.google.gson.*;

public class Wxhw {
   public String getWx(String zipCode) {
	 JsonElement jse = null;
     String wxReport = null;
     final String API_KEY = "7b56b3f8437bce02f1835a9695e2fc12"; 
     
     try {
        // Construct WxStation API URL
	    URL wxURL = new URL("http://api.openweathermap.org/data/2.5/weather?q="
					+ zipCode + ",US"
					+ "&units=imperial&appid="
                    + API_KEY);

         // Open the URL
	     InputStream is = wxURL.openStream(); // throws an IOException
         BufferedReader br = new BufferedReader(new InputStreamReader(is));
		 // Read the result into a JSON Element
		 jse = new JsonParser().parse(br);
      
		 // Close the connection
		 is.close();
		 br.close();
      }
      catch (java.io.FileNotFoundException fnfe) {
         //fnfe.printStackTrace();
         System.out.println("ERROR: No cities match your search query: " + zipCode);
      }
	  catch (java.io.UnsupportedEncodingException uee) {
	     uee.printStackTrace();
	  }
	  catch (java.net.MalformedURLException mue) {
          mue.printStackTrace();
	  }
	  catch (java.io.IOException ioe) {
		  ioe.printStackTrace();
	  }
      
	  if (jse != null) {
         // Build a weather report
         String name = jse.getAsJsonObject().get("name").getAsString();
         wxReport = formatDisplay("Location", name);
      
         int time = Integer.valueOf(jse.getAsJsonObject().get("dt").getAsString());
         Date date = new Date(time*1000L);
         SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         String timestamp = jdf.format(date);
         wxReport = wxReport + formatDisplay("Time", timestamp);

         JsonArray obs1 = jse.getAsJsonObject().get("weather").getAsJsonArray();
         String conditions = obs1.get(0).getAsJsonObject().get("description").getAsString();
         wxReport = wxReport + formatDisplay("Weather", conditions);
      
         String temp = jse.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsString();
         wxReport = wxReport + formatDisplay("Temperature", temp, "(F)");
     
         String windS = jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsString();
         wxReport = wxReport + formatDisplay("Wind", windS, "(MPH)");
     
         String windD = jse.getAsJsonObject().get("wind").getAsJsonObject().get("deg").getAsString();
         wxReport = wxReport + formatDisplay("Wind Direction", windD, "(degrees)");
     
         String pressure = String.format("%.2f",Double.valueOf(jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsString()) * 0.02953);
         wxReport = wxReport + formatDisplay("Pressure", pressure, "(inHG)");
	  } // end if
      
      return wxReport;
   }
   
   public String formatDisplay(String label, String value) {
      return String.format("%-20s", label + ":") + String.format("%-20s", value) + "\n";
   }
   
   public String formatDisplay(String label, String value, String units) {
      value = value + " " + units;
      return String.format("%-20s", label + ":") + String.format("%-20s", value) + "\n";
   }

   public static void main(String[] args) {
      Wxhw b = new Wxhw();
      String wx = null;
      Scanner sc = new Scanner(System.in);
      String newLine = "";
      // Check to see that we have at least one argument on the command line
      if ( args.length == 0 ){
         System.out.println("Please enter at least one zipcode:");
         newLine = sc.nextLine().trim();
         // System.out.println(newLine); // Test print for user input
         if (newLine.length() != 0) {
            String[] newArgs = newLine.split(" ", -1);
            main(newArgs);
         }
         else {
            System.out.println("Goodbye!");
         }
      } // end if
      // Take each argument and define it
      else {
         for (int i=0; i<args.length; i++) {
            wx = b.getWx(args[i]);
            if ( wx != null )
      		    System.out.println(wx);
         } // end for
      } // end else
   } // end main
} // end Wxhw